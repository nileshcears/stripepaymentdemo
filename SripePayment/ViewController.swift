//
//  ViewController.swift
//  SripePayment
//
//  Created by iOS_1 on 06/09/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import Stripe
import SwiftyJSON
import BraintreeDropIn

class ViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var paymentTextField: STPPaymentCardTextField!
    
    @IBOutlet var txtCard: UITextField!
    @IBOutlet var txtMMYY: UITextField!
    @IBOutlet var txtCVV: UITextField!

    var chargableAmount:Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentTextField?.font = txtAmount.font
        txtAmount.text = "1"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /*
        let cardParams = STPCardParams.init()
        cardParams.number = "4242424242424242"
        cardParams.expMonth = 12
        cardParams.expYear = 22
        cardParams.cvc = "123"
        paymentTextField?.cardParams = cardParams*/
        
        chargableAmount = 1
        if chargableAmount == 0.0 {
            txtAmount?.becomeFirstResponder()
        } else {
            paymentTextField?.becomeFirstResponder()
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        btnPay?.isUserInteractionEnabled = textField.isValid
    }

    @IBAction func btnPay(_ sender: Any) {
        showDropIn(clientTokenOrTokenizationKey: "")
/*
        let address: STPAddress = STPAddress()
        address.city = "Surat"
        address.country = "India"
        address.line1 = "Varachha, Surat"

        let card: STPCardParams = STPCardParams()
        card.name = "Test Stripe Payment"
        card.number = paymentTextField.cardNumber
        card.expMonth = paymentTextField.expirationMonth
        card.expYear = paymentTextField.expirationYear
        card.cvc = paymentTextField.cvc
        card.address = address

        if chargableAmount >= 1 {
            print("Loading...")
            paymentTextField?.resignFirstResponder()
            self.view.endEditing(true)
            STPAPIClient.shared().createToken(withCard: (paymentTextField?.cardParams)!, completion: {(_ token: STPToken?, _ error: Error?) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription ?? "Error is nil")
                }
                
                print("Payment Successful")
                print(token?.tokenId ?? "Not Found")
            })
        }*/
    }
    
    @IBAction func txtAmountValueChanged(_ sender: UITextField) {
        sender.text = sender.text?.replacingOccurrences(of: "..", with: ".")
        if sender.text == "" {
            chargableAmount = 0
        }else{
            if let amount = sender.text?.float(){
                chargableAmount = amount
            }else{
                chargableAmount = 0
            }
            
        }
        print("Your card will be charged for \(chargableAmount) $")
    }

    //BRAINTREE INTIGRATION
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
                let paymentNonce: String = result.paymentMethod?.nonce ?? ""
                print(paymentNonce)
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    /*
    func api_getPaymentToken(){
        
        LoadingHud.showDefaultHUD()
        HttpRequestManager.sharedInstance.requestWithPostJsonParamWithParseData( endpointurl:PaymentAPI + APIGetToken, service:APIGetToken, parameters:[:], keyname:kData as NSString, message:"", showloader:true, responseData: { (error, data, responseDic) -> Void in
            
            if let err = error
            {
                hideloader()
                showMessageWithRetry((err.localizedDescription), buttonTapHandler: { _ in
                    self.api_getPaymentToken()
                })
                return
            }
            else if data != nil
            {
                let thedata = responseDic
                if(thedata != nil)
                {
                    if (thedata?.count)! > 0
                    {
                        let status = thedata?[kStatus] as? Int ?? 0
                        print(status)
                        if(status == 1){
                            self.paymentToken = thedata?["data"] as? String ?? ""
                            print(self.paymentToken)
                        }else{
                            showMessage(thedata![kMessage] as! String)
                        }
                    }
                }
            }
            LoadingHud.dismissHUD()
        })
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

